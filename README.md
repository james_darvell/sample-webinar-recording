# Sample Webinar Recording #

The first section of the first webinar.  Also, a sample submitted to Udemy for approval.

### Technology ###

This project is developed using open source tools, which means it's easy to collaborate.

* I'm using [Audacity](http://www.audacityteam.org/) to edit sound files.
* I'm using [Blender](https://www.blender.org/) as a non-linear video editor.

### Project Composition ###

The project consists of a series of assets (that are stored in the assets/ directory) and a .blend file (stored in the projects/ directory). 

### Version Control ###

I'm using Git as a version control system.

### Contribution guidelines ###

* Fork the repo
* Make a branch
* Pull and merge the master branch before submitting your revisions
* Issue a pull request.

That said, I'm currently working independantly on this project, so I don't anticipate many pull requests...

### Contacts ###

If you have questions or comments, contact me via email: jgdarvell@gmail.com